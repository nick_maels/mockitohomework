package com.endava.internship.mocking.service;

import static org.assertj.core.api.Assertions.*;

import com.endava.internship.mocking.model.Payment;
import com.endava.internship.mocking.model.Status;
import com.endava.internship.mocking.model.User;
import com.endava.internship.mocking.repository.PaymentRepository;
import com.endava.internship.mocking.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {

    @Mock
    ValidationService validationServiceMock;

    @Mock
    UserRepository userRepositoryMock;

    @Mock
    PaymentRepository paymentRepositoryMock;

    @InjectMocks

    PaymentService paymentServiceMock;

    @Captor
    ArgumentCaptor<Payment> captor;

    @BeforeEach
    void setUp() {

    }

    @Test
    void testCreatePayment() {
        Integer id = 1;
        Double amount = 2.5;
        User user = new User(id, "John", Status.ACTIVE);
        Payment payment = new Payment(user.getId(), amount, "Message");

        when(userRepositoryMock.findById(user.getId())).thenReturn(Optional.of(user));
        when(paymentRepositoryMock.save(captor.capture())).thenReturn(payment);

        paymentServiceMock.createPayment(id, amount);

        verify(validationServiceMock).validateUserId(user.getId());
        verify(validationServiceMock).validateAmount(amount);
        verify(validationServiceMock).validateUser(user);
    }

    @Test
    void testEditMessage() {
        Integer id = 1;
        Double amount = 2.5;
        Payment payment = new Payment(id, amount, "Message");
        String newMessage = "New message";

        paymentServiceMock.editPaymentMessage(payment.getPaymentId(), newMessage);

        verify(validationServiceMock).validateMessage(newMessage);
        verify(validationServiceMock).validatePaymentId(payment.getPaymentId());
        verify(paymentRepositoryMock).editMessage(payment.getPaymentId(), newMessage);

    }

    @Test
    void getAllByAmountExceeding() {
        List<Payment> paymentsList = new ArrayList<>();
        paymentsList.add(new Payment(1,2.4,"Message"));
        paymentsList.add(new Payment(2,4.5,"Message_Two"));
        when(paymentRepositoryMock.findAll()).thenReturn(paymentsList);

        assertThat(paymentServiceMock.getAllByAmountExceeding(2.5).isEmpty()).isFalse();
    }
}
